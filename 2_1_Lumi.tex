\chapter{Luminosity measurements at CMS}

\section{Definition of luminosity}
\subsubsection{Cross section for beam-target interaction}
When a particle beam of density $n_1$ and velocity $\vec{v}_1$ hits a target with particle density $n_2$, the density of the interaction rate can be expressed as
\begin{equation}
    \label{eq:1}
    \pdv{N}{V}{t}=\sigma_{int}\abs{\vec{v}_1}n_1n_2.
\end{equation}
The parameter $\sigma_{int}$ is called the interaction cross section.
It carries the probability for that interaction and has the units of area - either \si{\centi\meter\squared} or $\si{b}=\SI{e-28}{\centi\meter\squared}$.
The total collision cross section (for all possible events) corresponds to the probability of collision.
The term cross section was first coined in times when particles were thought of as solid spheres, making the probability of their collision dependent on their actual physical cross sections.

\subsubsection{Luminosity of a stationary target collider}
Equation \ref{eq:1} can be expressed as 
\begin{equation}
    \dv{N}{t} = \iiint_V\sigma_{int}\abs{\vec{v}_1}n_1n_2
        \dd{x}\dd{y}\dd{z} = \mathcal{L}\sigma_{int}
        \label{eq:LumiDef},
\end{equation}
where $\mathcal{L}$ is the instantaneous luminosity - a global measure of the event rate. 
It is measured in \si{\centi\meter^{-2}.s^{-1}} or \si{b^{-1}.s^{-1}}.
Knowing one interaction cross section and measuring its event rate, the luminosity can be obtained.
Cross sections for other interactions can then be determined from their event rates.
Measuring luminosity only requires an apparatus that counts a quantity linear to the number of interactions (and therefore to luminosity), in other words can measure a rate
\begin{equation}
    R = \mathcal{L}\sigma_{vis},
    \label{eq:Rate}
\end{equation}
where the linear coefficient $\sigma_{vis}$ is called the visible cross section of the detector and is dependent on the size, position relative to the interaction point, and active material of the device.
The calibration of an instrument for luminosity measurements (luminometer) consists of the estimation of this constant.

\section{Luminosity of colliding beams}
\subsubsection{Head-on collisions of non-bunched beams}
The density of the interaction rate of two colliding bunches with particle densities $n_1$ and $n_2$ and velocities $\vec{v}_1$ and $\vec{v}_2$ was derived by W.C. Middelkoop and A. Schoch\cite{Kinematic_Derivation_Middelkoop:300882}
 using both a coordinate transformation and a more elegant Lorentz invariant formulation first described by Møller\cite{Moller}. 
 The total cross section $\sigma$ is assumed to be Lorentz invariant:
\begin{equation}
    \pdv{N}{V}{t}=
    \sigma n_1n_2\qty
    [
        (\vec{v}_1-\vec{v}_2)^2 - 
            \frac{(\vec{v}_1\cross\vec{v}_2)^2}{c^2}
    ]^{1/2}. 
\end{equation}
This means the luminosity, assuming non-bunched beams, is
\begin{equation}
    \mathcal{L} = \iiint_V n_1n_2\qty
        [
            (\vec{v}_1-\vec{v}_2)^2 - 
                \frac{(\vec{v}_1\cross\vec{v})^2}{c^2}
        ]^{1/2}\dd{x}\dd{y}\dd{z},
\end{equation}
and in the case of head-on collisions ($\vec{v}_1=-\vec{v}_2$) becomes
\begin{equation}
    \mathcal{L} = 2\iiint_V n_1n_2\dd{x}\dd{y}\dd{z}.
\end{equation}

\subsubsection{Bunched beams}
\begin{figure}
    \vspace{-80pt}
    \centering
    \includegraphics[width=.95\linewidth]{colliding_bunches2.png}
    \vspace*{-40pt}
    \caption{Colliding bunches time parametrization}
    \label{fig:col_bunches}
    \nocite{LumiConcept}
    \vspace{-10pt}
\end{figure}
The case of bunched beams warrants the introduction of the accelerator orbit frequency $f$ and the number of bunches per beam $N_b$, as well as the number of particles in the colliding bunches $N_1$ and $N_2$.
The bunched pattern also requires a time dependence.
Such can be introduced along the z axis (see \autoref{fig:CMS_coord}) with the parameter $s_0=ct$, illustrated in \autoref{fig:col_bunches}
Now the expression for luminosity becomes
\begin{equation}
    \mathcal{L} = 2N_1N_2fN_b
        \iiiint\rho_1(x_1,y_1,z_1,-s_0)
        \rho_2(x_2,y_2,z_2,s_0)
        \dd{x}\dd{y}\dd{z}\dd{s_0},
    \label{eq:LumiDens}
\end{equation}
where $\rho_{1,2}$ are the time dependent beam density distributions.
%%%%% EXTRA: Probably not going to use this
% From a physics point of view,  the two most important parameters of a collider are the energy of the collision (center of mass energy, $\sqrt{s}=\SI{13}{\tera\electronvolt}$ at the LHC) and the number of events it produces, characterized by luminosity\cite{LumiConcept}.
% Instantaneous luminosity is defined as the number of events (or rate) of a certain type per unit of time divided by the production cross section for that event.
% \[\mathcal{L}=\frac{\dot{N}}{\sigma_{p}}\label{eq:1}\]
% \\