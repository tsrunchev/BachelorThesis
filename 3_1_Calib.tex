\chapter{Calibration of luminometers}
\label{chap:LumiCal}

\section{Luminosity of Gaussian beams}
It is usually assumed beam density distributions are factorizable and have Gaussian profiles in all directions. The beam density distribution for beam $i$ becomes
\begin{equation}
    \rho_i(x_i,y_i,z_i,\pm s_0)=\rho_{xi}(x)\rho_{yi}(y)\rho_{z_i}(z\pm s_0)
    \label{eq:factored}.
\end{equation}
And so
\begin{equation}
    \mathcal{L} = 2N_1N_2fN_b\iiiint
        \rho_{x1}(x)\rho_{y1}(y)\rho_{z1}(z+s_0)
        \rho_{x2}(x)\rho_{y2}(y)\rho_{z2}(z-s_0)
        \dd{x}\dd{y}\dd{z}\dd{s_0},
    \label{eq:LumiDensFact}
\end{equation}
where the factored terms are
\begin{equation}
    \rho_{xi}=\frac{1}{\sigma_{xi}\sqrt{2\pi}}
                \exp[-\frac{(x-\mu_{xi})^2}{2\sigma_{xi}^2}],
                \label{eq:rhox}
\end{equation}
\begin{equation}
    \rho_{yi}=\frac{1}{\sigma_{y1}\sqrt{2\pi}}
                \exp[-\frac{(y-\mu_{yi})^2}{2\sigma_{yi}^2}],
\end{equation}
\begin{equation}
    \rho_{zi}=\frac{1}{\sigma_z\sqrt{2\pi}}
                \exp[-\frac{(z\pm s_0)^2}{2\sigma_z^2}],
    \label{eq:rhoz}
\end{equation}
where $\sigma_{xi,yi,z}$ are the corresponding beam widths and it is assumed the width in the $z$ direction is the same.
Equations \ref{eq:rhox}-\ref{eq:rhoz} are Gaussian integrals, meaning
\begin{equation}
    \int_{-\infty}^{\infty}e^{-x^2}\dd{x}=\sqrt{\pi}.
\end{equation}
Putting them into \autoref{eq:LumiDensFact} and integrating over $z$ and $s_0$:
\begin{equation}
    \begin{split}
    \mathcal{L} = \frac{2N_1N_2fN_b}{8\pi^2\sigma_{x1}\sigma_{x2}\sigma_{y1}\sigma_{y2}}
        \iint
        \exp[-\frac{(x-\mu_{x1})^2}{2\sigma_{x1}^2}-\frac{(x-\mu_{x2})^2}{2\sigma_{x2}^2}]
        \times\\
        \exp[-\frac{(y-\mu_{y1})^2}{2\sigma_{y1}^2}-\frac{(y-\mu_{y2})^2}{2\sigma_{y2}^2}]
        \dd{x}\dd{y}.
    \label{eq:LumiDensGaus}
    \end{split}
\end{equation}
To evaluate the integral over $x$ (equivalent to the one over $y$), a coordinate system where $\mu_{x1}=0$ is chosen.
Then $\mu_{x2} = d_x$ is the distance between the centers of the two beam distributions.
The integral in question is then
\begin{equation}
    \int_{-\infty}^\infty\frac{1}{2\pi\sigma_{x1}\sigma_{x2}}
        \exp[-\frac{x^2}{2\sigma_{x1}^2}]\exp[-\frac{(x-d_x)^2}{2\sigma_{x2}^2}]\dd{x}.
\end{equation}
This, as described in detail in Appendix A of \cite{Castro:316609}, becomes
\begin{equation}
    \begin{split}
    \frac{1}{2\pi\sigma_{x1}\sigma_{x2}}
    \exp[-\frac{d_x^2}{2(\sigma_{x1}^2+\sigma_{x2}^2)}]
    \int_{-\infty}^\infty
    \exp[-\frac{\sigma_{x1}^2+\sigma_{x2}^2}{2\sigma_{x1}\sigma_{x2}}
    \left(x-\frac{\sigma_{x1}^2d_x}{\sigma_{x1}^2+\sigma_{x2}^2}\right)^2]\dd{x}
    =\\=
    \frac{1}{\sqrt{2\pi(\sigma_{x1}^2+\sigma_{x2}^2)}}
        \exp[-\frac{d_x^2}{2(\sigma_{x1}^2+\sigma_{x2}^2)}].
    \end{split}
\end{equation}
The last equality comes again from integrating a Gaussian function. Now the expression for luminosity becomes
\begin{equation}
    \begin{split}
    \mathcal{L}=\frac{N_1N_2fN_b}
                    {2\pi\sqrt{(\sigma_{x1}^2+\sigma_{x2}^2)
                    (\sigma_{y1}^2+\sigma_{y2}^2)}}
                \exp[-\frac{d_x^2}{2(\sigma_{x1}^2+\sigma_{x2}^2)}
                     -\frac{d_y^2}{2(\sigma_{y1}^2+\sigma_{y2}^2)}]
                =\\= \frac{N_1N_2fN_b}
                {2\pi\sqrt{(\sigma_{x1}^2+\sigma_{x2}^2)
                (\sigma_{y1}^2+\sigma_{y2}^2)}} \mathcal{E}.
    \label{eq:LumiSep}
    \end{split}
\end{equation}

Calibration at the LHC is done under special beam optics making the beam relatively larger, while keeping the beams well centered, rendering the exponent term in \autoref{eq:LumiSep} $\mathcal{E}\approx 1$, as shown in \autoref{table:optics}.

\begin{table}[h!]
    \begin{center}
    \caption{Comparison of beam optics and filling scheme between calibration (Fill 6016) and a "normal" physics fill (fill 6362). Ranges in values account for bunch-to-bunch variations and differences in planes.}
    \begin{tabular}{l|r|r}
        Conditions    & Calibration      & Physics        \\
        \hline
        $\beta^*$ [cm]                          &1917              &30                \\
        $\alpha_{crossing}$ [\si{\micro\radian}]&0                 &150               \\
        Colliding bunches                       &32                &1866              \\
        Protons per bunch                       &9$\cross 10^{10}$ &$12\cross 10^{10}$\\
        \multicolumn{3}{l}{Values here are illustrational:}\\
        \hline
        Beam width [\si{\micro\meter}]                &80-90             &12-14              \\
        Beam displacement ($d_x$) [\si{\micro\meter}] &0.3-2             &0.5-1              \\
        $\mathcal{E}$                                 &0.9999            &0.998
    \end{tabular}
    \label{table:optics}
    \end{center}
\end{table}

Using this, the luminosity during a calibration fill of the LHC should be just
\begin{equation}
    \mathcal{L}=\frac{N_1N_2fN_b}
                    {2\pi\sqrt{(\sigma_{x1}^2+\sigma_{x2}^2)
                    (\sigma_{y1}^2+\sigma_{y2}^2)}}.
    \label{eq:LumiVdM}
\end{equation}


As mentioned earlier, calibration of a luminometer consists of estimating its visible cross section.
Putting \autoref{eq:LumiVdM} into the connection between detector hit rates ($R$), detector visible cross section ($\sigma_{vis}$) and luminosity (\autoref{eq:Rate}) the visible cross section can be expressed from beam parameters as
\begin{equation}
    \sigma_{vis}=\frac{R}{\mathcal{L}}=\frac
        {2\pi R\sqrt{(\sigma_{x1}^2 + \sigma_{x2}^2)
        (\sigma_{y1}^2+\sigma_{y2}^2)}}
        {N_1N_2fN_b}.
    \label{eq:sigmavis}
\end{equation}

A commonly used expression for luminosity can be derived if the beams are also assumed to be the same size in the $x$ and $y$ directions ($\sigma_{x1}=\sigma_{x2}=\sigma_x$ and $\sigma_{y1}=\sigma_{y2}=\sigma_y$):
\begin{equation}
    \mathcal{L}=\frac{N_1N_2fN_b}{4\pi\sigma_x\sigma_y}.
    \label{eq:LumiSimple}
\end{equation}
Then for the visible cross section:
\begin{equation}
    \sigma_{vis}=\frac{R}{\mathcal{L}}=\frac
        {4\pi R\sigma_x\sigma_y}
        {N_1N_2fN_b}.
    \label{eq:sigmavisSimple}
\end{equation}

These are the general expressions most often used for calculating instantaneous luminosity from beam parameters.
% They are also the theoretical basis of using Van der Meer scans for detector calibration.
% Various corrections and systematic uncertainties are often applied to obtain a more precise measurement, accounting for beam-beam deflections, beam drift, crossing angle, non-factorizability of beam density functions, satellite and ghost charges, etc.


\section{Van der Meer method}
\begin{wrapfigure}{r}{.5\textwidth}
    \vspace{-30pt}
    \centering
    \includegraphics[width=.98\linewidth]{vdm_vis.png}
    % \vspace*{-40pt}
    \caption{Visualization of the movement of the two beams during a Van der Meer scan in the $x$ direction}
    \label{fig:vdm_vis}
    \vspace{-10pt}
\end{wrapfigure}

The visible cross section is a parameter of the detector that, for an ideally linear luminometer, is independent of beam characteristics.
Therefore it needs to be determined once every time the detector is installed and setup in a certain way.
This is usually done once a year in a special fill using the so called Van der Meer scans, named after Simon van der Meer who first described it in 1968\cite{vanderMeer:296752}.
The method consists of moving or "scanning" the two beams across each other in each direction on the collision plane ($x$ and $y$) in discrete steps of certain length, as illustrated in \autoref{fig:vdm_vis}.
Measurements are averaged over each step.\\

According to \autoref{eq:LumiSep}, assuming the beams are centered in one direction ($y$) while being scanned in the other ($x$), the resulting rate measurement averages $R$ should follow a Gaussian distribution with width $\Sigma_x = \sqrt{\sigma_{x1}^2+\sigma_{x2}^2}$, like so
\begin{equation}
    R(d_x,d_y=0) = \frac{\mathcal{L}}{\sigma_{vis}}
            = \frac{N_1N_2fN_b}{2\pi \Sigma_x \Sigma_y \sigma_{vis}}
             \exp[\frac{(d_x-\mu_x)^2}{2\Sigma_x}]
            = R_0\exp[\frac{(d_x-\mu_x)^2}{2\Sigma_x}].
\end{equation}

Here $R_0=R(d_x=0,d_y=0)$ is the peak rate in the detector and $\mu_x$ has been added to adjust for the beams not being perfectly centered when set to $d_x=0$ by the LHC.
Fitting this distribution in both the $x$ and $y$ directions with a Gaussian function gives the necessary parameters to calculate the visible cross section for the detector as
\begin{equation}
    \sigma_{vis} = \frac{\pi (R_{0x} + R_{0y}) \Sigma_x \Sigma_y}{N_1N_2fN_b}
    \label{eq:sigmavis_fw},
\end{equation}
where the average of the two peak rate estimations $R_{0x}$ and $R_{0y}$ has been taken.

A constant term can be added to account for any background during calibration (background is generally insignificant in normal physics conditions).
A sum of two Gaussian functions can also be used to provide a better fit as its variance is also the sum of the variances of the two Gaussians.
