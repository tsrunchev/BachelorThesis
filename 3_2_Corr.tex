\section{Corrections and uncertainties}
There are a lot of assumptions made in the previous section and not all possible effects are considered.
After the initial calibration outlined there, a multitude of studies are carried out to account for any possible discrepancies and each one of them yields a correction and/or an additional systematic uncertainty to the measurement.
Although these investigations are not themselves the subject of this thesis, they are briefly outlined here for completeness.
Detector-specific corrections are omitted.
A more detailed account can be found in the 2017 CMS Luminosity Physics Analysis Summary (PAS)\cite{CMS-PAS-LUM-17-004}.

\subsubsection*{Length scale calibration}
This accounts for the differences in actual beam separation from the desired separation set by the LHC magnets.
To study these differences a set of special scans is conducted under calibration conditions and the primary vertices positions reconstructed by the CMS Tracker are used to determine the beam spot position.
Two types of scans are used in CMS - one where the two beams are moved together while kept at a constant $1\sigma$ separation and one where a small 3-step scan is made with one beam across the other at multiple positions. A Gaussian function is used to fit each three points of the latter and its mean value is taken as the beam spot position.
The measured beam spot positions are then linearly fitted against the nominal ones to determine the size of the correction and its uncertainty.

\subsubsection*{Orbit drift}
Orbit drift is the potential movement of the LHC orbit during a VdM scan.
CMS Beam Position Monitoring systems (BPM) are used to measure this position before, after and in the middle of each scan.
The correction is applied to each half of a scan and is calculated as the linear fit between the two orbit position data points surrounding it.

\subsubsection{The $x$-$y$ correlations}
This corrections aims to account for any non-factorizability of the beam density distributions in the $xy$ plane.
A pair of "imaging" scans are conducted in each direction, where one beam is scanned across, while the other is stationary. 
This allows the measurement of the stationary beam's particle density distribution.
A more complex model including a third, negative Gaussian added to the double Gaussian function is used to fit the data.
The correction is derived through multiple Monte Carlo simulations of a VdM scan using the measured beam densities.

\subsubsection{Beam-Beam effects}
Two effects caused by beam-beam interactions are corrected for.\\
The first is the lateral electric deflection between the two beams that affects their separation.
It is calculated as described in \cite{BeamBeam} and needs a $\Sigma$ measurement input that means every scan needs to be fitted twice - before and after this correction.
This is one of the largest corrections as it is usually at the percent level and is applied to almost any VdM-like analysis.\\
The second effect is called dynamic-$\beta$ effect and account for the defocusing of the two beams by each other.
It is a correction applied to the rates and determined through beam transport simulations\cite{anton}.

\subsubsection{Bunch current normalization}
Two possible corrections are accounted for in the bunch current measurement.
The first is from the normalization of the  Fast Beam Current Transformers (FBCT) per-bunch beam current measurements (whose relative uncertainty is negligible) to the DC current transformers (DCCT) that measure all of the charge in the LHC accurately\cite{DCCTFBCT}.\\
The second is the "satellite" and "ghost" charges correction.
The former are charges that are in the colliding bunch slot, but not in the correct RF bucket, while the latter are outside of any filled bunch slot.
They are measured by the DCCT but they do not contribute to measured luminosity.

\subsubsection{Linearity and stability}
The ideal detector has a completely linear, constant and reliable response to luminosity.
Of course, real-world luminometers have various issues with both, and even after correcting each individual detector, some discrepancies are still evident.
A systematic uncertainty is taken to account for these from comparisons between each pair of detectors.
A new approach using short VdM-like scans in each fill has been used to correct for such effects and reduce the systematic uncertainty and will be discussed further in Chapter \ref{chap:emittance}.
