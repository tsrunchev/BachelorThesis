\section{Automation wrapper}
As mentioned in the previous chapter, in 2017 the amount of data that needed to be analysed vastly increased.
It used to be an (ideally) once per year calibration of 4-5 luminometers from several scans with 32 colliding bunches, which made the manual configuration, single thread, intermediary data output in multiple format of the core VdM framework a viable option.
In 2017 there was a need to start analyzing so-called "emittance scans" for various stability and linearity studies.
Emittance scans are short (7-9 steps of 10 seconds each, compared to 25 30-second-steps for the normal VdM scan) VdM-like scans conducted at the beginning and planned dump of each LHC Fill.
Additionally, a new readout system in operation for BCM1F as well as the need for per-channel analysis of both BCM1F and PLT increased the number of analysed detectors over 20 times.
Results from analysis also needed to be sent to a web page for monitoring purposes.\\

This required a new automated, threaded and optimized approach for the VdM framework.
Backwards compatibility was kept as much as possible and so the automation was done through several new scrips that acted as a wrapper of sorts to the VdM Driver script (the core framework).
File naming is self-resolved and all files are saved and looked for in configurable "Automation" folder (not necessarily named "Automation").

\subsection{The Configurator script}
This is the script that automates all the manual input of DIP-related and metadata to the VdMDriver configuration (\autoref{appendix:VdMDriverConfig}). Step by step, it:
\begin{enumerate}
    \item Gets the DIP data from the hd5 file and converts it to a csv file of the old format used by the core framework;
    \item Finds where scans begin and end (previously done by hand);
    \item Loads template configurations (json configurations like \autoref{appendix:VdMDriverConfig} with various parts removed and replaced with keywords);
    \item Uses all that to make configurations for the VdMDriver script and save them to 
    \verb|{Automation folder}/autoconfigs/{scanname}/| 
    under the format\\
    \verb|{luminometer}_{corr1_corr2_etc}_{fit}_driver.json|;
    \item Makes a second configuration if the Beam-Beam correction is included  one without the correction to get a $\Sigma$ measurement that is a necessary input for the Beam-Beam correction.
\end{enumerate}

\subsection{The RunAnalysis script}
\begin{enumerate}
    \item Finds pre-made (either by hand or by configurator) configurations according to the naming convention described in the previous paragraph;
    \item Calls VdMDriver with those configurations, automatically running twice with different configurations if the Beam-Beam correction is included;
    \item Does error handling and logging;
    \item Loads the FitResults and LumiCalibration files into Pandas\cite{mckinneypandas} data frames and returns them.
\end{enumerate}
This script can (or perhaps should) be made part of the VdMDriver script in the future.

\subsection{The PostVdM script}
As already mentioned, results from analysis needed to be uploaded to a database and shown on a web page for monitoring purposes.
The script:
\begin{enumerate}
    \item Takes FitResults and LumiCalibration Pandas data frames and extracts $\Sigma$, $R_{peak}$, $\mu$, $\chi^2$, $\sigma_{vis}$ and SBIL;
    \item Calculates averages over all bunch crossings for each variable;
    \item Fits $\sigma_{vis}$ against SBIL as a linear function for lead (first in train) and train bunches;
    \item Puts all this data plus some metadata into a JSON file and saves it to the main folder of the scan;
    \item Optionally publishes to one of three web service endpoints (main, test and per-channel) which then save the data to a database that is the source for web monitor pages;
    \item Determines channel number (if analysed data is from a single channel) from luminometer name using regular expression \verb|([A-Z1_]*[A-Z]+)_?([0-9]+)|.
\end{enumerate}

\subsection{The AutoAnalysis script}
This script is the main entry point when using the automation wrapper of the framework.
It can analyse a single HD5 file, a pair of them (in case two scans of an XY pair are separated accidentally) or it can check a folder periodically and analyse single HD5 files as they arrive ("watcher" mode).
It also uses a JSON configuration file (see \autoref{appendix:AutoAnalysisConfig}) that sets:
\begin{itemize}
    \item folder paths (Automation folder, logs folder, data source folder;
    \item the minimum number of steps in a scan to use a double Gaussian function;
    \item the number of seconds to wait before checking the data source folder when in watcher mode;
    \item the maximum number of threads the script is allowed to use at the same time;
    \item the names of the tables in the HD5 files (ratetables) of "main" luminometers that should be posted to main web monitoring page.
\end{itemize}

The rest of the options are done through command line arguments as follows:
\begin{itemize}
    \item \verb|-f {hd5 file path}| When a single file needs to be analysed outside of the watcher loop;
    \item \verb|-c {folder path}| Data source folder, in case different from configuration file;
    \item \texttt{-b} Apply Beam-Beam correction;
    \item \texttt{-t} Only analyse the luminometers from the config file;
    \item \texttt{-p} Post one-time analysis to web monitoring (watcher automatically posts);
    \item \verb|-f2 {hd5 file path}| Second scan of the scan pair (if scan pair is split);
    \item \texttt{-d} Force double Gaussians instead of single;
    \item \texttt{-pdf} Make PDF files with visualizations of Beam-Beam correction and fitted functions;
    \item \texttt{-l} Make logs for the fitting (both fit function scripts and Minuit logs);
    \item \texttt{-ls} Apply length scale correction (only applicable if you have length scale data);
    \item \texttt{-bg} Apply background correction.
\end{itemize}

Once the path to an HD5 file (or two) is received, the script:
\begin{enumerate}
    \item Creates the Automation folder at the specified path if it does not exist, as well as 3 folders inside it - \textbf{Analysed\_Data, autoconfigs} and \textbf{dipfiles}.
    \item Loads DIP data from the \textbf{vdmscan} table in the table and uses the remapping function from the Configurator script and saves the result to the dipfiles folder.
    \item Gets the timestamps of starts and ends of scans using the Configurator script and treats them as XY scan pairs from there on out.
    \item Finds all luminosity tables in the HD5 file by their names using the regular expression \verb|(.*)_?lumi[a-z]*_?(.*)| where the capitalized version of the first group is then considered the name of the luminometer and the second part should contain a channel number and algorithm tag in the case of BCM1F.
    \item Decides on whether to use a double or a single Gaussian fit depending on he number of steps and whether the \texttt{-dg} option was used.
    \item Names each scan as \verb|{Fill}_{startdate&time}_{enddate&time}|.
    Folders with this name are created in autoconfigs and Analysed\_Data folders.
    \item Calls runAnalysis script once over a single luminometer to get the common files (scan file, beam currents file, etc) created using the subprocess\cite{Python2} library and the Popen function.
    \item Runs the rest of the found luminometers in batches of 10 unless the \texttt{-t} option was used, in which case only the ones from the configuration file get analysed. The maximum number of simultaneous  runs is configurable in the AutoAnalysis JSON configuration file (Appendix \ref{appendix:AutoAnalysisConfig}).
    \item At the end of each batch calls the PostVdM script on the finished luminometers.
    By default, in watcher mode everything gets published to the web monitoring, and if the \texttt{-p} option was invoked when analysing a single file.
    \item Gives full access rights on all files created to all users (since this is done within a completely separated cluster).
\end{enumerate}