\chapter{Emittance scans for CMS luminosity calibrations}
\label{chap:emittance}
\setlength{\columnsep}{5pt}
\begin{figure}
    \vspace{-10pt}
    \centering
    \includegraphics[width=.6\linewidth]{LumiOverFill.png}
    \includegraphics[width=.39\linewidth]{LumiScan.png}
    \caption{$\mu$ values for the PLT during the whole fill 6241 (left) and a zoom-in on the late emittance scan(right)}
    \label{fig:FillLumi}
    \vspace{-10pt}
\end{figure}
\begin{wrapfigure}{r}{.45\textwidth}
    \vspace{-50pt}
    \centering
    \includegraphics[width=\linewidth]{EmittanceScan6362_PLT_BX10_X.pdf}
    \vspace{-30pt}
    \caption{Example of a single Gaussian fit to an emittance scan in Fill 6362}
    \label{fig:EmittanceFits}
    \vspace{-30pt}
\end{wrapfigure}
In 2015 the LHC started conducting small Van-der-Meer-like scans just after declaring stable beams ("early scan") and before any planned beam dump ("late scan") each fill .
\autoref{fig:FillLumi} shows the luminosity during a fill, the emittance scans are visible as short dips at the beginning and at the end of the fill.
Each scan had 7 steps of 10 seconds and reached a maximum separation of $\pm 3\sigma$.
The purpose of these scans was to derive plane-by-plane, bunch-by-bunch emittances and orbit offsets using CMS luminosity measurements, which is why they are now called "emittance scans"\cite{Hostettler:2207311,Hostettler:2017ghs}.\\

In 2017 data from emittance scans were analysed similarly to data from calibration VdM scans and the results were used for stability and linearity studies of the CMS luminosity detectors.
During the year the number of steps in a scan was increased to 9 to allow for a better beam shape measurement.
\autoref{fig:EmittanceFits} shows an example of the fits to such data, with a zoom-in on a late scan on the right.
Each point on the plot is an average of the measurements made during the step (5-6 measurements each).\\

\vspace{-20pt}
\section{Non-linearity of a luminometer's response}
\begin{figure}
    \centering
        \vspace{-20pt}
        \includegraphics[width=.49\linewidth]{sigvis_6362_0.png}
        \includegraphics[width=.49\linewidth]{sbil_6362_0.png}

        \caption{Per-BX $\sigma_{vis}$ (left) and SBIL (right) measurements by the PLT from the early scan in LHC Fill 6362}
        \label{fig:EmittancePerBunch}
        \vspace{-30pt}
\end{figure}

The system setup by BRIL using the automated framework described in Chapter \ref{chap:FW} outputs per-bunch-crossing (per-BX) measurements of the visible cross section and single bunch instantaneous luminosity (SBIL).
\autoref{fig:EmittancePerBunch} shows such measurements for the PLT detector.
It is important to note the difference between leading (first in train)  and the rest of the bunches in a train, as well as the large range of SBIL across the bunch crossings in a single fill - between 6 and 10 $Hz/\mu b$ during early scans and between 2 and 4 $Hz/\mu b$ during late ones, depending on the duration of the fill.\\

\begin{wrapfigure}{r}{.45\textwidth}
    \vspace{-40pt}
    \centering
    \includegraphics[width=\linewidth]{sigvis(SBIL)_6362_bothscans}
    \vspace{-20pt}
    \caption{PLT $\sigma_{vis}$ as a function of SBIL in Fill 6362, both scans}
    \label{fig:Sigvis(SBIL)}
    \vspace{-10pt}
\end{wrapfigure}
This large range allows for studies of the dependence of $\sigma_{vis}$ on SBIL, which should be a constant for an ideal luminometer.
As seen in \autoref{fig:Sigvis(SBIL)}, that is not the case - in fact two separate linear dependencies can be determined for the leading and train bunches.
The dependency is more pronounced in the early scan (the group of points at high SBIL on the plot) but can be traced down to the results from the late scan (low SBIL points on the plot).
A study of this dependency has been conducted on a per-scan basis.
\autoref{fig:Slope} shows the calculated slope in each scan as a function of Fill number (left) and as a function of the average SBIL calculated in that scan (right).
It is noticeable that the effect is generally larger on leading bunches, but the difference grows smaller at higher SBIL.\\

\begin{figure}
    \centering
        \includegraphics[width=.49\linewidth]{slope(Fill).png}
        \includegraphics[width=.49\linewidth]{slope(SBIL).png}

        \caption{Per-scan slope of PLT $\sigma_{vis}(SBIL)$ as a function of LHC Fill number (left) and SBIL (right)}
        \label{fig:Slope}
\end{figure}

A separate measurement can be made by fitting average $\sigma_{vis}$ as a function of average SBIL per scan.
As shown in \autoref{fig:Linearity}, the results agree well between the two methods.\\

A data-driven simulation was carried out that showed a linear dependence between the linear term in $\sigma_{vis}(SBIL)$ and the nonlinear (quadratic) term in the detector's response function, with the ratio between the two terms in the range of $\approx 0.3-0.7$.
Further studies need to be done to be conducted for a better estimate of this parameter.
However, it is important to note this gives a new method for estimating the non-linearity in a detector's response and that it is self-contained, as it does not rely on comparison to other detectors.

\begin{figure}[H]%{.65\textwidth}
    \centering
    \vspace{-20pt}
    \includegraphics[width=.85\textwidth]{PLTLinearity6300-6371-1.jpg}
    \caption{PLT $\sigma_{vis}$(SBIL) in Fills 6300-6371. This is a stable period for the PLT}
    \vspace{-10pt}
    \label{fig:Linearity}
\end{figure}
\pagebreak
\section{Luminometer stability}
It has already been established that emittance scans cross section and SBIL measurements are strongly affected by detector response quality.
It is therefore not surprising they can be used for stability monitoring.
\autoref{fig:Stability} shows how average $\sigma_{vis}$ per scan can reveal long term stability problems - the highlighted areas were periods where the PLT was losing efficiency.
The problem was fixed by raising the operational high voltage.
As described in Chapter \ref{chap:FW}, all emittance scans are automatically analysed and the results are published to a database and displayed on web monitoring pages to help shifters notice long term issues quicker.
For the offline luminosity measurement a stability correction was derived from the average cross section for each fill.

\begin{figure}[H]%{.75\textwidth}
    \centering
    \includegraphics[width=.85\textwidth]{stability.png}
    \caption{Average PLT visible cross section per fill for all data from 2017. Error bars signify standard deviation of the per BX measurements in the average. Regions of loss of efficiency are highlighted.
    }
    \label{fig:Stability}
\end{figure}
