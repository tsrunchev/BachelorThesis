import numpy
from matplotlib import pyplot as plt

plt.scatter([i+1 for i in range(25)],[-3. + i*6./24 for i in range(25)],marker='x', label='Beam 1')
plt.scatter([i+1 for i in range(25)],[3. - i*6./24 for i in range(25)]
            ,marker='o',facecolors='none',edgecolors='red', label='Beam 2')
plt.xlabel('Time [steps]')
plt.ylabel('x coordinate [$\sigma_x$]')
plt.legend()
plt.show()
