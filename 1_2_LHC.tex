\chapter{Introduction}
\vspace{-12pt}
\section{The Large Hadron Collider}
The Large Hadron Collider (LHC)\cite{LHC}\nocite{LHCWhy} is the largest particle accelerator collider in the world.
Its circumference spans \SI{27}{\kilo\meter} in the French and Swiss countryside near Geneva, between \SI{45}{\meter} and \SI{170}{\meter} underground.
The tunnel was constructed for the Large Electron Positron (LEP)\cite{LEP} collider in 1984-1989.
The beams rotate in opposite directions along two parallel beam pipe rings and gets accelerated by radio frequency (RF) cavities in which the electromagnetic field oscillates at \SI{400}{\mega\hertz}.\\
% Protons that arrive on time with the ideal energy experience no acceleration, while all others get accelerated or decelerated to match the ideal parameters.


The LHC machine is designed to accelerate two proton beams moving in opposite directions from \SI{450}{\giga\electronvolt} to \SI{7}{\tera\electronvolt} and then collide them at four interaction points (IPs), around which the four big experiments on the LHC are built - ATLAS, ALICE, CMS and LHCb.
At these energies the protons in the beam complete 11245 orbits around the LHC.
This and the RF cavities frequency determine the harmonic number of the LHC to be ~35640\cite{bunches}.
This is the number of virtual positions (buckets) in the LHC circumference that could be filled with particles to be accelerated.
The beams are separated into \SI{25}{\nano\second}-spaced bunches, making for a total 3564 bunch slots.
Some of these are reserved for the abort gap - the time needed for the beam dumping magnets to kick in.
The LHC gets filled with 12 "supercycles" of the 234 bunches in the Super Proton Synchrotron (SPS) \cite{lhc_running}.
The maximum number of occupied buckets or bunches in the LHC is 2808.
Each bunch contains around \SI{1.15e11} protons at the start of a nominal fill.\\

The initial proton beams are prepared by a series of accelerator facilities on the CERN site (\autoref{fig:LHC}).
First the linear accelerator LINAC 2 accelerates protons up to \SI{50}{\mega\electronvolt}, then the Proton Synchrotron Booster accelerates them to \SI{1.4}{\giga\electronvolt} and injects them into the Proton Synchrotron (PS), where they are further accelerated to \SI{26}{\giga\electronvolt} before finally reaching the Super Proton Synchrotron (SPS), which accelerates them to \SI{450}{\giga\electronvolt}.
For about a month each year the LHC also accelerates and collides heavy ions - lead-lead, proton-lead, or most recently - xenon-xenon.
The maximum center-of-mass energy achieved is \SI{5.02}{\tera\electronvolt} per colliding nuclear pair for lead\cite{Sirunyan:2017isk}  and \SI{5.44}{\tera\electronvolt} per colliding nuclear pair for xenon\cite{Schaumann:2291215}.\\

\begin{figure}%[H]%[h!]
    \vspace{-20pt}
    \centering
    \includegraphics[width=.95\linewidth]{LHC_Inject.jpg}
    \caption{CERN Accelerator complex schematic}
    \label{fig:LHC}
    \nocite{https://stfc.ukri.org/research/particle-physics-and-particle-astrophysics/large-hadron-collider/cern-accelerator-complex/}
    \vspace{-10pt}
\end{figure}

% \begin{figure}%{r}{0.5\textwidth}
%     \vspace{-20pt}
%     \includegraphics[width=.95\linewidth]{StandardModel.png}
%     \centering
%     \caption{The Standard Model of Elementary Particles}
%     \label{fig:SM}
%     \vspace{-10pt}
% \end{figure}

The main goal of the LHC project was to test various predictions and theories of particle physics, mainly deciding between the Standard Model 
%(\autoref{fig:SM})
and some other Higgsless model to explain the electroweak symmetry breaking giving mass to the W and Z bosons.
On July 4th 2012 in a joint announcement the ATLAS and CMS experiments confirmed they had both observed a particle with a mass around \SI{126}{\giga\electronvolt} that was consistent with the Higgs boson predicted by the Standard model at 5$\sigma$ significance \cite{HiggsCMS,HiggsATLAS}.
Studies in the next years have further shown evidence that this is in fact a Higgs boson.
It is not yet certain if this is the Standard Model Higgs boson or one of multiple Higgs bosons from other theories beyond the SM.

% \section{Experiments at the LHC}
% \paragraph{ATLAS}
% ATLAS (A Toroidal LHC ApparatuS) is one of the two general-prupose experiments at the LHC and also physically the largest such detector.\nocite{ATLAS}
% \paragraph{ALiCE}
% \paragraph{CMS}
% \paragraph{LHCb}
% \paragraph{Other experiments}
\section{The CMS Experiment}
\begin{figure}
    \vspace{-20pt}
    \centering
    \includegraphics[width=.95\linewidth]{cms.png}
    \caption{CMS Schematic}
    \label{fig:CMS}
    \nocite{http://cms.web.cern.ch/news/cms-detector-design}
    \vspace{-10pt}
\end{figure}
\begin{figure}
    \vspace{-20pt}
    \centering
    \includegraphics[width=.95\linewidth]{cms_coordinate_system.png}
    \caption{CMS coordinate system}
    \label{fig:CMS_coord}
    \nocite{https://wiki.physik.uzh.ch/cms/latex:example_spherical_coordinates}
    \vspace{-10pt}
\end{figure}
The Compact Muon Solenoid\cite{CMS} (shown in \autoref{fig:CMS}) is one of the two general purpose experiments at the LHC (along with ATLAS).
It is \SI{21}{\meter} long and \SI{14.6}{\meter} wide and weighs 12500 ton.
The detector was designed with requirements for good performance in:
\begin{description}[labelindent=1cm]
    \item[$\bullet$] Muon identification and momentum resolution and unambiguous charge determination when $p<$\SI{1}{\tera\electronvolt};
    \item[$\bullet$] 1\% dimuon, diphoton and dielectron mass resolution at \SI{100}{\giga\electronvolt};
    \item[$\bullet$] Charged-particle momentum resolution and reconstruction efficiency;
    \item[$\bullet$] Electromagnetic energy resolution, geometric coverage, $\pi^{0}$ rejection, and photon and lepton isolation at high luminosities;
    \item[$\bullet$] Missing-transverse-energy and dijet-mass resolution.
\end{description}
\par
The coordinate system (\autoref{fig:CMS_coord}) used in CMS has its $x$-axis pointing towards the center of the LHC, $y$-axis pointing vertically upward and $z$-axis pointing along the beamline.
The azimuthal angle $\phi$ is measured in the $x$-$y$ plane while the polar angle $\theta$ is measured from the $z$-axis.
Often in particle physics the parameter pseudorapidity $\eta=\nobreak\ln{(\tan{\theta/2})}$ is used instead of the latter as it is Lorenz invariant.
\par
\subsection*{Magnet}
The namesake of the CMS experiment is the solenoid - a superconducting niobium titanium coil measuring \SI{13}{\meter} long and \SI{7}{\meter} wide, creating a \SI{3.8}{\tesla} magnetic field.
This is needed to curve the trajectory of high energy particles, allowing the measurement of their momentum.

\subsection*{Tracker}
The innermost part of the CMS is the tracking system comprised of multiple layers of silicon pixel (\SI{100x150}{\micro\meter}) and silicon strips (\SI{80x180}{\micro\meter}) modules.
They measure the positions of passing charged particles, which are used to reconstruct the particle trajectories, giving their momenta.

\subsection*{Calorimeters}

The calorimeters are designed to absorb and measure the energy of as many particles as possible.
There are two calorimeters - the Electromagnetic Calorimeter (ECAL) and the Hadronic Calorimeter (HCAL).

\paragraph{ECAL}
Next come the calorimeters which measure the energies of most particles and them.
The Electromagnetic Calorimeter (ECAL) deals with electrons and photons.
It is made from $PbWO_{4}$ scintllating crystals which are read out with Avalanche Photo Detectors and covers pseudorapidity up to $|\eta|<3.0$.
In the endcaps there is also a preshower system of smaller (\SI{2}{\milli\meter} compared to \SI{3}{\centi\meter} wide) Si-Pb sensors for $\pi^{0}$ diphoton decay rejection.

\paragraph{HCAL} \label{par:HCAL}
The Hadronic Calorimeter (HCAL) is built from alternating layers of brass (absorber) and plastic scintillators.
Hadrons may interact with the absorber producing multiple secondary particles which then also interact, resulting in a "shower" of particles.
These then pass through the alternating layers causing scintillation which is then transferred to strategically placed readout boxes through tiny optical "wavelength-shifting fibres" and clear optic tables.
Multiple layers that are chosen so they receive roughly the same number of particles get optically added to form "towers".
This allows for energy measurements.
The HCAL can also be used to deduce production of otherwise undetectable particles (e.g.
neutrinos) from imbalance in the momentum and energy measured along the transverse direction.\\
HCAL is the outermost part of the detectors that is still inside the solenoid, but it also has several additional layers just outside the magnet (HO, outer barrel).
The rest of it is organised into the barrel (HB), endcap (HE) and forward (HF) sections, each covering a different range of pseudorapidities, the latter going as high as $|\eta|=5.0$.
The HF is also used for luminosity measurements as described in \autoref{ssec:HF}.

\subsection*{Muon chambers}\label{muonChambers}
An iron yoke outside the magnet is employed to guide and contain the magnetic field.
It is interleaved with the muon detectors in three layers spanning out \SI{14}{\meter} in diameter.
Only muons and weakly interacting particles pass through the calorimeters and the "return yoke".\\ 
The outermost part of the CMS detector is the muon chambers designed to identify muons and measure their momentum.
It is crucial to the search for the Higgs boson as the ZZ and ZZ* decay channels can further decays into four muons which allow for the best mass resolution as they are less susceptible to radiation losses.

\paragraph{The Drift Tube (DT)} system is deployed in the barrel, where background levels are low, covering $|\eta|<1.2$.
Each tube is \SI{4}{\centi\meter} wide and contains a stretched wire in a gas volume.
When a charged particle passes it ionizes the gas and the resulting electrons get pulled by the field to the positive wire.
The time it takes for the electron to reach the wire combined with the position along the wire where it hits gives two coordinates of the particle's position.

\paragraph{The Cathode Strip Chambers (CSCs)} are employed in the endcaps, where there are high muon and background rates, $0.9<|\eta|<2.4$.
They are arrays of positively charged wires crossed with negatively charged ones within a gas volume.
Charged particles again knock electrons towards the anodes and ions toward the cathodes, giving two coordinates per detection.

\paragraph{The Resistive Plate Chambers (RPCs)} provide a fast, robust and independent trigger system parallel to those of the other two muon systems ($|\eta|<1.6$), but with worse position resolution.
They consist of two parallel oppositely charged high-resistance plates separated by gas.
Muons cause electron avalanches in the gas which after a precise time delay get picked up by external metallic strips.
