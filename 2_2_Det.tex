\section{Luminometers at CMS}
\begin{figure}
    \vspace{-30pt}
    \centering
    \includegraphics[width=.95\linewidth]{lumi_pos.png}
    \vspace*{-10pt}
    \caption{BRIL detector positions in CMS}
    \label{fig:lumi_pos}
    \nocite{Guthoff:2017ibf}
\end{figure}
\paragraph{The Beam Radiation Instrumentation and Luminosity (BRIL)}\cite{Guthoff:2017ibf} project is responsible for luminosity, beam timing and machine induced background (MIB) measurements at CMS.
The positions of all the BRIL-operated detectors in the CMS can be seen in \autoref{fig:lumi_pos}.
This thesis is focused on the luminosity measurement aspects of the project's activities.
\subsection{Offline and Online luminosity}
Luminosity measurements are separated by the way they are obtained and analysed.
Online luminosity is an initial estimate of instantaneous (or integrated over a short period, e.g. an LHC orbit) luminosity.
Detectors providing this measurement need to have fast readout systems allowing for momentary analysis.
\paragraph{Online luminosity} provided to CMS and the LHC is used for monitoring purposes and needs to have a reliable and stable system providing it.
An independent data acquisition system based on the CMS XDAQ\cite{XDAQ} software framework has been developed to allow luminosity measurements even when the central CMS data acquisition system is offline or has problems.
Due to their fast readout systems, online luminometers can also separate measurements per bunch crossing (BX), allowing for fine grained monitoring of the beam and bunch train structure studies.
\paragraph{Offline luminosity} is usually calculated at the end of the year when all the data is available, including general purpose detectors with more complicated readout systems not optimized for luminosity.
This is the most precise measurement, it is corrected for all known effects and used in all physics analyses done by the collaboration.
Its precision therefore directly affects the precision of all those results and is of crucial importance.

\subsection{Zero-counting algorithm}
Generally, a luminometer counts or measures a quantity that is linear to luminosity and therefore to the number of primary interactions.
However, luminometers often cannot distinguish between a single hit and multiple hits in the same sensor, prompting the use of the so-called "zero counting" method.
Since the number of hits should follow a Poisson distribution, the probability of having $n$ hits is
\begin{equation}
    p(n)=\frac{\mu^ne^{-\mu}}{n!}
\end{equation}
and so the mean value $\mu$ is
\begin{equation}
    \mu=-\ln{(p(0))}=-\ln{(1-p(\neq 0))}
\end{equation}
The probability of having no hits can therefore be used to calculate luminosity, given that the occupancy is low enough.

\subsection{PLT}
\begin{wrapfigure}{R}{.35\textwidth}
% \InsertBoxR{0}{
    % \begin{minipage}{.35\textwidth}
    \centering
    \vspace*{-20pt}
    \includegraphics[width=.98\linewidth]{plt.png}
    \vspace*{-20pt}
    \caption{Pixel Luminosity Telescope sensor arrangement}
    \vspace*{-10pt}
    \label{fig:plt}
    \nocite{http://www.bo.infn.it/sm/sm08/presentations/10-02a2/hits.pdf}
    % \end{minipage}
% }
\end{wrapfigure}
The Pixel Luminosity Telescope \cite{PLT} is a dedicated luminometer using phase-0 CMS pixel sensor detector modules and readout chips.
Each module has an active area of 8$\times$8 mm in the x-y plane and 80$\times$52 discrete pixel sensors.
The modules are arranged in columns of three along the z axis called telescopes (\autoref{fig:plt}) at a total distance of about \SI{7.5}{\centi\meter}, so that a track coming from the interaction point (IP) passes through all three modules.
Eight telescopes are installed around the beampipe at a radius of \SI{5}{\centi\meter} just outside each CMS pixel detector endcap (\SI{1.75}{\meter} from the IP on each side of the CMS) for a total of 16 telescopes (48 sensors).
This corresponds to a pseudorapidity of about $|\eta|=4.2$. A special "fast-or" mode of the readout chip is used, allowing for 40MHz readout (per bunch crossing measurements) by only showing whether any pixels in a sensor were hit.
A particle crossing through all three sensors in a telescope indicates a track from the IP and is called a "triple coincidence".
The rate of triple coincidences should be proportional to luminosity.
The zero-counting method is used here to measure luminosity.

\subsection{HF}\label{ssec:HF}
The Hadronic Forward calorimeter (HF) is also used for both offline and online luminosity measurements.
It is part of the HCAL (see \autoref{par:HCAL}).
It is positioned \SI{11.2}{\meter} away from the IP on each end of CMS and covers a pseudorapidity range of $3<|\eta|<5$, but only the four azimuthal rings with $3.5<|\eta|<4.2$ are used for luminosity measurement to minimize non-linear contribution, amounting to 864 towers.
Two separate algorithms have been used for HF luminosity measurements:
\begin{itemize}
    \item \textbf{HFOC} - This is the algorithm used in Run 1 and ever since.
    It uses the zero-counting method on the number of occupied towers in the HF.
    \item \textbf{HFET} - This algorithm was first used in 2017 and was expected to smaller nonlinearity.
    The main observable here is the sum of all the transverse energy deposited into the HF.
    It was considered the most reliable and stable throughout the year and so was the offline luminometer of choice for the year.
\end{itemize}
Lookup tables are used to calibrate ADCs to energy. 
In the central HF DAQ system these tables are regularly updated, but for luminosity they have been kept constant for each year and corrected by a degradation model provided by HF detector experts.

\subsection{BCM1F}
\begin{figure}
    \vspace{-20pt}
    \centering
    \includegraphics[width=.95\linewidth]{BCMPLTCarriage.png}
    \caption{BCM1F and PLT carriage}
    \label{fig:carriage}
    \nocite{KarachebanThesis}
    \vspace{-10pt}
\end{figure}
The Fast Beam Condition Monitor\cite{BCM1F} (BCM1F) was first commissioned in 2008 as a machine induced background (MIB) monitor.
It is installed in a strategic location at \SI{1.8}{\meter} from the IP (right next to the PLT, \autoref{fig:carriage}) at a radius of \SI{6.94}{\centi\meter} in order to separate incoming MIB and outgoing collision background by \SI{12.5}{\nano\second}.
This, combined with excellent time resolution (\SI{6.25}{\nano\second}) allows the detector to also be used for luminosity measurements, which can be corrected by the background measurement.
The detector has been upgraded multiple times since its initial installation and in the 2016-17 Extended Year-End Technical stop it was installed with 10 poly-crystalline chemical vapour deposition (pCVD) diamond sensors, 10 silicon sensors and 4 single crystalline CVD (sCVD) diamond sensors.
CVD crystals use two-pad metallization, while silicon ones use single pads, making a total of 38 readout channels.
Each channel is treated as a separate luminometer using the zero-counting algorithm on the number of hits per bunch crossing.

\subsection{CMS Tracker}
The innermost part of the CMS Tracker is comprised of a total of 66 million separate pixel detectors of size \SI{100x150}{\micro\meter}. 
The detector is not suitable for online luminosity monitoring since it is only on when "Stable Beams" have been declared and is reliant on the CMS data acquisition system\cite{PCC}.
The readout system is not designed for online luminosity measurements, but the detector can measure luminosity high precision.
For calibration purposes, in order to reach the required statistical precision, only 5 bunches are selected to be read out.
\paragraph{The Pixel Cluster Counting (PCC)}\cite{PCC,CMS-PAS-LUM-12-001} method is used for luminosity measurements using the CMS Tracker.
It provides a stable and reliable measurement due to the amount of data it provides and was the primary offline luminosity measurement source for 2015 and 2016, but problems in the power supply chain in the last part of the 2017 run significantly reduced the number of used modules.
The method is based on the assumption that, since the occupancy in the detector is at the per-mil level, the mean pixel clusters per bunch crossing is proportional to the number of interactions $\mu$ which is given by the proton-proton minimum bias cross-section and instantaneous luminosity as
\begin{equation}
    \mu=\frac{\sigma_{interaction}}{f}\mathcal{L}
\end{equation}
and, taking the linearity to the average number of clusters $\langle N_{clusters}\rangle$,
\begin{equation}
    \mathcal{L}=\frac{\langle N_{clusters}\rangle f}{\sigma_{pcc}}
\end{equation}
Where $\sigma_{pcc}$ is the visible cross section of the tracker with the PCC method and is determined through a Van der Meer scan (described in Chapter \ref{chap:LumiCal})
\paragraph{Vertex counting}
A second method for offline luminosity measurements using the tracker is with the reconstructed tracks, finding the primary interaction vertices and counting those.
At low luminosity this is one of the cleanest measurements, as it also has no background.
However, at high luminosity, collisions occur close to each other and become indistinguishable from tracks alone.
Thus, this method is only used in calibration fills to help measure beam profiles more accurately.
\subsection{The Drift Tubes}
The drift tubes (Section \ref{muonChambers}) use the rate of muon track stubs.
They do not provide per-bunch measurements and do not have the readout system necessary for Van der Meer calibration, however they have proven to be a very stable and linear luminosity measurement system that is used as an offline reference luminometer.
It is cross-calibrated to another luminometer (HFET in 2017) in order to be used as a luminometer.