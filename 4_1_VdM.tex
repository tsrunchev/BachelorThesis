\chapter{Van der Meer scan analysis framework}
\label{chap:FW}

Van der Meer scan analysis in the CMS collaboration is conducted using a software framework written in Python 2 and utilizing CERN's data analysis package ROOT\cite{ROOT} via the PyRoot\cite{PyROOT} library.
The framework was first developed for Run II of the LHC (starting 2015) as a centralized software package for the Van der Meer (VdM) analysis for CMS luminometers.\\

The framework takes data from BRIL DAQ and from the LHC through the Data Interchange Protocol (DIP)\cite{dip}, does some data manipulation, applies some corrections, fits one of several predefined Gaussian-like functions to the data and uses the resulting $\Sigma_{x,y}$ and $P_{x,y}$ to calculate the calibration constant for the detector via \autoref{eq:sigmavis_fw}.
Multiple data and visualization files are made in the process to allow further analysis and tests: average rates per step, average beam intensities per step, various corrections applied, fitted graphs, etc.\\

The framework was largely unchanged until 2017, when the amount of data to be analysed was vastly increased - from several dozens of fits for 3-4 detectors once a year, to a more or less daily run of several thousand fits for $>50$ detector channels - warranted a major overhaul of the system.
Backwards compatibility has been kept as much as possible by implementing a wrapper of sorts to the already developed framework, but large parts of it had to be redone to comply with new requirements.
Both the old\cite{github_old,gitlab_old} and the new\cite{gitlab_new,github_new} versions of the framework, as well as some simple documentation\cite{twiki_old,twiki_new_tech} are available online.\\

This chapter aims to document the inner workings and usage of both the updated "core" framework (as well as changes compared to original) and its "automation wrapper".

\section{Core framework}
\begin{figure}%[H]%[h!]
    \vspace{-20pt}
    \makebox[\textwidth][c]{
        \includegraphics[width=1.25\linewidth]{VdMFW_Diagram2.png}}
    % \centering
    \caption{Van der Meer Analysis Framework Core Diagram\\(log and visualization files not mentioned)}
    \label{fig:VdMFW}
    \vspace{-10pt}
\end{figure}
The core framework (visualized in \autoref{fig:VdMFW}) is ran using a large JSON\cite{JSON} configuration file (see example at \autoref{appendix:VdMDriverConfig}) for the main script, \textbf{vdmDriverII.py}, which in turn runs all other scripts.
The configuration file contains multiple separable configurations for each script which could be ran completely separately.
This generally requires some human input, like timestamps for beginnings and endings of scans, folder structure, file names, fit function choice, etc.
The basic process for the framework is:
\begin{enumerate}
    \item \textbf{Make scan file} - Get DIP data (step start and end timestamps, separations between beams, crossing angle, $\beta^*$, etc) either from BRIL DAQ data dumps(HDF5\cite{hdf5} files) or from the separate DIP data dump file (csv), format them properly and save to a csv (human-readable) and a json (easily computer readable).

    \item \textbf{Make rate file}
    \begin{enumerate}
        \item Get BRIL DAQ luminometer data - per bunch raw rates integrated over four "lumi-nibbles" (NB4). A nibble is equal to 4096 ($2^{12}$) LHC orbits or around \SI{364}{\milli \second}.
        \item Get BRIL DAQ beam data to determine the filling scheme (the colliding bunch numbers whose rates need to be saved).
        This is done using a threshold (currently $6\times 10^9$ protons per bunch) as sometimes there are low intensity bunches used for machine diagnostics that should not be included in averages.
        \item Take step start and end timestamps from scan file.
        \item Average the per-NB4 measurements over each step for each colliding bunch and calculate associated uncertainties.
        This used to be done as a standard error on the mean of the measurements, but in 2018 this was determined to often underestimate the uncertainty and a binomial error is now calculated for zero-counting algorithms.
    \end{enumerate}

    \item \textbf{Make beam currents file}
    \begin{enumerate}
        \item Get BRIL DAQ beam data and scan file.
        \item Average per-NB4, per-BX measurements of each beam intensity over each step.
        Uncertainty is assumed to be negligible.
    \end{enumerate}

    \item \textbf{Make correction files} - Corrections applied trough the framework are generally separated into two scripts - one that makes a data file outlining the correction and a second that reads that file and applies it during the graph-making process.

    \item \textbf{Make graph file}
    \begin{enumerate}
        \item Load \textbf{Input data reader} script to load the scan, rate and beam currents files.
        The input data reader loads all the data into memory multiple times with different ways of accessing it (by step, by bunch, etc.).
        \item Load \textbf{Correction Manager} - a plugin manager for the different correction scripts - and apply corrections. Currently supported corrections by the framework are:
        \begin{enumerate}
            \item \textbf{Beam-Beam correction}, accounting for lateral electric deflection of the beams, changing the real beam separation;
            \item \textbf{Length scale correction}, accounting for differences between the nominal beam separation set by the LHC magnets and the actual separation;
            \item \textbf{Ghost and satellite charges corrections} - satellite charges are charges in the colliding bunch but not the colliding RF bucket, ghost charges are charges outside of any nominally filled bunch slots;
            \item \textbf{Background correction} - first used for 2017 calibration, this correction estimates noise and beam induced background from the abort gap and from filled non-colliding bunches, respectively, thus removing a constant term from the fit function.
        \end{enumerate}
        \item \textbf{Normalize rates} - divide each rate measurement by the corresponding beam currents and multiply by $10^{22}$ (the latter is just for readability).
        \item Save resulting normalized rates and corresponding separation values to a new "graphs" file, each BX is a separate graph.
    \end{enumerate}
    
    \begin{figure}[tbh]
        \centering
        \includegraphics[width=0.45\textwidth]{{Images/VdMScanFit_PLT_BX1783_X1.pdf}}
        \includegraphics[width=0.45\textwidth]{{Images/VdMScanFit_PLT_BX1783_Y1.pdf}}
        \vspace{-20px}
        \caption{Normalized rates and the resulting fitted double Gaussian scan curves as a function of the beam separation ($\Delta$) for a single bunch as recorded by PLT for a scan in the $x$ (left) and $y$ direction (right).
        Background subtraction has been applied to the raw data.
        Beam-beam deflection and Length scale corrections have been applied.}
        \label{fig:vdmfits} 
    \end{figure}
    \item \textbf{Fit} Load another plugin manager called "Fit Manager" which loads different fit scripts (most commonly a single Gaussian (SG), a double Gaussian (DG) and either of those plus  a constant).
    Each fit script creates a ROOT TF1 function and fits each graph to such a function using Minuit.
    The resulting fit parameters are saved to a csv table.
    Optionally a visualization of each "shape" (set of points) and its fit is made and saved to a PDF file.
    \autoref{fig:vdmfits} shows a scan pair example for a single bunch from the 2017 VdM scan program calibrating the PLT.

    \item \textbf{Calculate visible cross section} Get the resulting Gaussian parameters (peak rates, $\Sigma$) from the X and Y scan and use them to calculate the calibration constant using \autoref{eq:sigmavis_fw}.
\end{enumerate}

Every step is optional and can be set through the configuration file.
For the scan, rates and beam currents files the default setting is "False", and they get automatically made if they are not already available.
This is done to simplify the automation process.
